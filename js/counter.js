// Get the DOM elements
var counterForm = document.getElementById("counter-form");
var counterFormInput = document.getElementById("max-counter-value");
var counterDisplay = document.getElementById("current-counter-value");
var fingersDisplay = document.getElementById("fingers");
var toesDisplay = document.getElementById("toes");

// Set initial values
var currentCounterValue = 0;
var maxCounterValue = null;
var counter = setInterval(Update, 1000);
var counterRunning = true;

// Main update loop
function Update(){
	if (currentCounterValue >= maxCounterValue && maxCounterValue != null) {
		StopCounter();
	}
	
	if (counterRunning) {
		currentCounterValue++;
		counterDisplay.innerHTML = currentCounterValue;	
		
		// 	Make sure that the correct box highlights based on the counter value
		if (currentCounterValue % 3  == 0) {
			fingersDisplay.classList.add("block-active");
		} else fingersDisplay.classList.remove("block-active");
		
		if (currentCounterValue % 5  == 0) {
			toesDisplay.classList.add("block-active");
		} else toesDisplay.classList.remove("block-active");
	}
};

function StartCounter() {
	counter = setInterval(Update, 1000);
	counterRunning = true;
}

function StopCounter() {
	clearInterval(counter);
	counterRunning = false;
}

function ResetCounter() {
	counterForm.reset();
	maxCounterValue = null;
	RestartCounter();
}

function RestartCounter() {
	StopCounter();
	currentCounterValue = 0;
	counterDisplay.innerHTML = currentCounterValue;
	StartCounter();
}

// Update the max count when the user changes the value in the input box.
function UpdateMaxCounterValue() {
	maxCounterValue = counterFormInput.value;
}